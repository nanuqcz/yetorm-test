<?php

use Nette\Database\Table\Selection;


/**
 * ArticleRepository
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class ArticleRepository extends YetORM\Repository
{
	
	public function findAll() 
	{
		return $this->createCollection( $this->getTable()->order('id DESC') );
	}

}
