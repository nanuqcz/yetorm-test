<?php


/**
 * ArticleCategory entity
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class ArticleCategory extends YetORM\Entity
{

	public function getId()
	{
		return $this->row->id;
	}


	public function getName()
	{
		return $this->row->name;
	}

}
