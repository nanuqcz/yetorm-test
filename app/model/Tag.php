<?php

use YetORM\EntityCollection;


/**
 * Tag entity
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Tag extends YetORM\Entity
{

	public function getId()
	{
		return $this->row->id;
	}


	public function getName()
	{
		return $this->row->name;
	}


	public function getDeleted()
	{
		return $this->row->deleted;
	}

}
