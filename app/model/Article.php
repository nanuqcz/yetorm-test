<?php

use YetORM\EntityCollection;


/**
 * Article entity
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Article extends YetORM\Entity
{

	public function getId()
	{
		return $this->row->id;
	}


	public function getCategory()
	{
		if ($categoryRow = $this->row->article_category)
			return new ArticleCategory($categoryRow);
		else
			return FALSE;
	}


	public function getTitle()
	{
		return $this->row->title;
	}


	public function getContentHtml()
	{
		return $this->row->content_html;
	}


	public function getTags()
	{
		$article2tag = $this->row->related('article_2_tag')
							->where('tag.deleted', 0);

		return new EntityCollection($article2tag, $entityClass = 'Tag', $referencedTable = 'tag');
	}

}
