<?php
namespace App;

use ArticleRepository;


/**
 * Homepage presenter.
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class HomepagePresenter extends BasePresenter
{
	/** @var ArticleRepository */
	private $articleRepository;


	public function injectArticleRepository(ArticleRepository $articleRepository)
	{
		$this->articleRepository = $articleRepository;
	}


	public function renderDefault()
	{
		$this->template->articles = $this->articleRepository->findAll();
	}

}
